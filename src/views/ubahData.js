import React, {Component} from "react";
import { SafeAreaView, View, Text, TextInput, StyleSheet, Pressable} from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import AsyncStorage from '@react-native-async-storage/async-storage'

export class ubahData extends Component{
    constructor(props) {
        super(props);
        this.state = {
            name: ['']
        };
      }
    addNew = () => {
        this.setState( prevState =>
                ({
                    name:[...prevState.name, '']
                })
        )
    }

    changeValue = (value, index) =>{
        this.setState( prevState => {
            let arr = prevState.name
            arr[index] = value
            return ({
                name:arr
            })
        })
    }

    removeName = () => {
        let deltName = this.state.name;
        deltName.pop();
        this.setState({ deltName });
    }

    updateData = async ()=>{
        try {
            let data = []
            const value = await AsyncStorage.getItem('namaUser')
            if (value == null) {
                data = 
                    this.state.name
                
            }else{
                data = JSON.parse(value)
                    data.unshift(
                        this.state.name
                    )
                    console.log(data)
            }
            const jsonValue = JSON.stringify(data)
            await AsyncStorage.setItem('namaUser', jsonValue)
            this.props.navigation.push('Payment Detail')
            } catch(e) {
                console.log(e);
            }
    }
    render(){
        return(
            <SafeAreaView style={styles.wrapper}>
                <Text style={styles.textHeader}>Data Tamu</Text>

                {
                        this.state.name.map((customName, index)=>{
                            return(
                                <View key={index} style={styles.formInput}>

                                <View style={styles.dropDownView}>
                                    <Text style={styles.textMr}>MR v</Text>
                                </View>
            
                                <TextInput style={styles.inputNameView} placeholder="Insert Your Name" placeholderTextColor='grey'
                                    onChangeText={(text) => this.changeValue(text, index)}
                                    value={customName}>
                                </TextInput>
            
                                <Pressable onPress={this.removeName}>
                                    <Ionicons style={styles.refundIcon} name="trash-outline"></Ionicons>
                                </Pressable>
                                
                            </View>
                            )
                        })
                    }

                    <Pressable style={styles.newUser} onPress={this.addNew}>
                        <Text style={styles.textNewUser}>+ Tambah Data Tamu</Text>
                    </Pressable>
                    
                    <Pressable style={styles.updateData} onPress={this.updateData}>
                                <Text style={styles.textUpdateData}>Simpan</Text>
                    </Pressable>

            </SafeAreaView>

        )
    }
}

export default ubahData

const styles = StyleSheet.create({
    wrapper:{
        height:'100%',
        backgroundColor:'white'
    },
    textHeader:{
        fontSize:25,
        margin:10,
        fontWeight:'bold',
        color:'#0072BB',
    },
    formInput:{
        flexDirection:'row',
        justifyContent:'flex-end',
        margin:10,
    },
    inputNameView:{
        height:50,
        width:250, 
        borderRadius:10,
        margin:5,
        flexDirection:'column',
        alignSelf:'center',
        borderWidth:1,
        borderColor:'lightgrey',
        color:'black'
    },
    dropDownView:{
        height:50,
        width:80, 
        borderRadius:10,
        flexDirection:'column',
        alignSelf:'center',
        borderWidth:1,
        borderColor:'lightgrey',
    },
    textMr:{
        color:'#0072BB',
        fontSize:25,
        margin:5,
        fontWeight:'bold',
        alignSelf:'center'
    },
    refundIcon:{
        fontSize:50,
        color:'red',
    },
    newUser:{
        alignSelf:'flex-end',
        margin:10,
        borderBottomWidth:1,
        borderColor:'orange',
        alignSelf:'center',
    },
    textNewUser:{
        color:'orange',
        fontSize:20,
    },
    updateData:{
        height:50,
        width:375,
        borderRadius:5,
        backgroundColor:'orange',
        justifyContent:'center',
        alignSelf:'center',
        bottom:1,
        position:'absolute',
    },
    textUpdateData:{
        fontSize:25,
        alignSelf:'center',
        color:'white',
        fontWeight:'bold',
    },
})