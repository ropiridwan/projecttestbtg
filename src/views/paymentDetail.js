import React, {Component} from "react";
import { View, Text, StyleSheet, Pressable, ScrollView, Image, Dimensions} from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import axios from '../axios/config'
import AsyncStorage from '@react-native-async-storage/async-storage'

export class paymentClass extends Component{
    constructor(props) {
        super(props);
        this.state = {
            dataServe: [],
            dataLocal: ['']
        };
      }

    componentDidMount(){
        axios.get('pGLq0d1eLK')
        .then(response=>{
            this.setState({dataServe:[response.data.values.data.hotel_booking_detail.bh_passenger_obj.bh_contact_user]})
            console.log({dataServe:response.data.values.data.hotel_booking_detail.bh_passenger_obj.bh_contact_user})
        })
        .catch(err=>{
            console.log(err)
        })

        const getData = async ()=>{
            try {
                const value = await AsyncStorage.getItem('namaUser')
                if(value != null) {
                    const namaUser = JSON.parse(value)
                    this.setState({dataLocal:namaUser})
                    console.log({dataLocal:namaUser})
                }
             } catch(e) {
                 //error reading
             }
            }
            getData()
    }

    ubahDataTamu = () =>{
        this.props.navigation.navigate('Tambah Data Tamu')
    }
    
    RemoveData = async () => {
        try {
            await AsyncStorage.removeItem('namaUser')
        } catch(e) {
          // remove error
        }
    }

    render(){
        return(
            <ScrollView>
            <View style={styles.wrapView}>
            
                <View style={styles.headerPage}>
                    <View style={styles.wrapCircle}>
                        <Text style={styles.circleContents}>1</Text>
                    </View>
                    <Text  style={styles.orderDetail}>Detail Pesanan</Text>

                    <View style={styles.stripStyle}>
                        <Text>-----</Text>
                    </View>

                    <View style={styles.wrapCircle}>
                        <Text style={styles.circleContents}>2</Text>
                    </View>
                    <Text  style={styles.orderPayment}>Pembayaran</Text>
                </View>
               

                <View style={styles.wrapView2}>
                    <View style={{flexDirection:'column'}}>
                        <Text style={styles.orderDetail2}>Detail Pesanan</Text>
                        
                        <View style={styles.viewBedroom}>
                            <Image style={styles.viewImage} source={{uri:'https://www.hotelkristal.com/wp-content/uploads/sites/221/2017/12/2-BEDROOM-DELUXE-BEDROOM.jpeg'}}/>
                            
                            <View style={styles.wrapDescBed}>
                                <Text style={styles.viewAddres}>Novotel Tangerang</Text>
                                <Text style={styles.textDescView}>Executive Suit Room with Breakfast</Text>
                                <Text style={styles.textDescView}>1 Kamar * Quardruple * 2 Tamu * 10 Malam</Text>
                            </View>
                        </View>
                        <View style={styles.checkView}>
                            <Text style={styles.textCheck}>Check-In</Text>
                            <Text style={styles.textDate}>30 November 2020</Text>
                        </View>

                        <View style={styles.checkView}>
                            <Text style={styles.textCheck}>Check-Out</Text>
                            <Text style={styles.textDate}>14 Desember 2020</Text>
                        </View>
                        <View style={styles.refundView}>
                            <Ionicons style={styles.refundIcon} name="refresh-circle-outline"></Ionicons>
                            <Text style={styles.textRefund}>Dapat direfund Jika dibatalkan</Text>
                        </View>
                        
                    </View>
                </View>

                <Text style={styles.textPemesan}>Detail Pemesan</Text>
                    {
                        this.state.dataServe.map((dataS, index)=>{
                            return(
                            <View key={index} style={styles.userView}>
                                <Text style={styles.nameUser}>{dataS.name}</Text>
                                <View style={styles.wrapAddres}>
                                    <Text style={styles.textAddres}>{dataS.email}</Text>
                                    <Text style={styles.textChanged}>Ubah</Text>
                                </View>
                            
                                <Text style={styles.textPhone}>{dataS.phone}</Text>
                            </View>
                            )
                        })
                    }
                
                {/* DISINI SAYA MASIH BELUM BEGITU PAHAM KENAPA EROR UNTUK MULTIPLECHOICE NYA,
                APABILA INGIN GET API BISA DI COMMMENT TERLEBIH DAHULU MULTIPLECHOICE NYA.
                KARENA KALAU TIDAK DI COMMENT NANTI ADA ERR WARN DAN TIDAK DAPAT GET API*/}
                {/* <MultipleChoice
                    chosenColor='#0072BB'
                    chosenTextColor='#0072BB'
                    direction={'column'}
                    choices={['Saya Memesan Untuk Sendiri',
                    'Saya Memesan Untuk Orang Lain']}
                /> */}

                <View style={styles.multipleChoic}>
                    <View style={styles.wrapCircle1}>
                        <View style={styles.wrapCircleOut}>
                            <View style={styles.wrapCircle2}>
                            </View>
                        </View>
                        <Text  style={styles.orderDetail}>Saya Memesan Untuk Sendiri</Text>
                    </View>

                    <View style={styles.wrapCircle1}>
                        <View style={styles.wrapCircleOut}>
                            <View style={styles.wrapCircle2}>
                            </View>
                        </View>
                        <Text  style={styles.orderPayment}>Saya Memesan Untuk Orang Lain</Text>
                    </View>
                </View>

                <Text style={styles.textPemesan}>Data Tamu</Text>

                <ScrollView>
                    {
                        this.state.dataLocal.map((item, index)=>{
                            return(
                                <View key={index} style={styles.visitorData}>
                                <Ionicons style={styles.refundIcon} name="person-circle-outline"></Ionicons>
                                <Text style={styles.textData}>{item}</Text>
                            </View>
                            )
                        })
                    }
                </ScrollView>
                
                    <Pressable style={styles.changeMenu} onPress={this.ubahDataTamu}>
                        <Text style={styles.textChangeMenu}>Ubah Data Tamu</Text>
                    </Pressable>

                    {/* <Pressable onPress={this.RemoveData}>
                     <View style={{height:30,
                                    width:100,
                                    backgroundColor:'#324A59',
                                }}>
                            <Text style={{alignSelf:'center', margin:5, color:'white'}}>Hapus Data</Text>
                    </View>
                </Pressable> */}
            </View>
        </ScrollView>
        )
    }
}

export default paymentClass

const height = Dimensions.get('screen').height

const styles = StyleSheet.create({
    wrapView:{
        height:height,
        backgroundColor:'white',
        flexDirection:'column',
    },
    wrapView2:{
        height:260,
        borderTopWidth:1,
        borderBottomWidth:1,
        borderColor:'lightgrey'
    },
    headerPage:{
        flexDirection:'row',
        left:70,
        alignItems:'center',
        alignSelf:'center',
    },
    wrapCircleOut:{
        width:30,
        height:30,
        borderRadius:15,
        backgroundColor:'white',
        borderWidth:1,
        borderColor:'lightgrey'
    },
    wrapCircle:{
        width:20,
        height:20,
        borderRadius:10,
        backgroundColor:'#0072BB',
    },
    wrapCircle2:{
        width:20,
        height:20,
        borderRadius:10,
        backgroundColor:'#0072BB',
        alignSelf:'center',
        margin:3,
    },
    circleContents:{
        fontSize:15,
        color:'white',
        alignSelf:'center',
    },
    orderDetail:{
        fontSize:15,
        color:'black',
        fontWeight:'bold',
        marginLeft:10,
    },
    orderDetail2:{
        fontSize:25,
        margin:10,
        color:'black',
        fontWeight:'bold'
    },
    orderPayment:{
        fontSize:15,
        color:'black',
        marginLeft:10,
    },
    stripStyle:{
        borderBottomColor:'blue',
        borderTopWidth:3,
        margin:15,
        marginTop:30,
    },
    viewBedroom:{
        height:70,
        width:375,
        borderRadius:5,
        borderWidth:1,
        borderColor:'lightgrey',
        alignSelf:'center',
        flexDirection:'row',
    },
    viewImage:{
        height:60,
        width:60,
        borderRadius:10,
        alignSelf:'center',
        margin:5,
    },
    wrapDescBed:{
        flexDirection:'column',
        margin:5,
    },
    viewAddres:{
        color:'black',
        color:'#0072BB',
    },
    textDescView:{
        color:'black',
        color:'lightgrey',
    },
    checkView:{
        justifyContent:'space-between',
        flexDirection:'row',
    },
    textCheck:{
        fontSize:20,
        margin:10,
        color:'black',
        fontWeight:'bold',
    },
    textDate:{
        fontSize:15,
        margin:10,
        color:'lightgrey',
    },
    refundView:{
        flexDirection:'row',
        alignSelf:'flex-end',
        alignContent:'center'
    },
    refundIcon:{
        fontSize:25,
        margin:10,
        color:'orange',
    },
    textData:{
        color:'black',
        fontSize:15,
        fontWeight:'bold',
        margin:10,
        alignSelf:'center'
    },
    textRefund:{
        fontSize:20,
        margin:10,
        color:'orange',
    },
    textPemesan:{
        fontSize:20,
        marginLeft:10,
        color:'black',
        fontWeight:'bold',
    },
    userView:{
        height:90,
        width:375, 
        borderRadius:10,
        flexDirection:'column',
        margin:20,
        borderWidth:1,
        borderColor:'lightgrey',
    },
    nameUser:{
        color:'black',
        color:'black',
        fontWeight:'bold',
        marginLeft:10,
    },
    wrapAddres:{
        flexDirection:'row',
        justifyContent:'space-between',
        margin:10,
    },
    textAddres:{
        color:'black',
        color:'lightgrey',
    },
    textChanged:{
        color:'#0072BB',
        borderBottomWidth:1,
        borderColor:'#0072BB',
    },
    textPhone:{
        color:'black',
        color:'lightgrey',
        marginLeft:10,
    },
    visitorData:{
        height:50,
        width:"100%", 
        borderRadius:10,
        margin:2,
        flexDirection:'row',
        borderWidth:1,
        borderColor:'lightgrey',
    },
    changeMenu:{
        alignSelf:'flex-end',
        margin:10,
        borderBottomWidth:1,
        borderColor:'#0072BB',
    },
    textChangeMenu:{
        color:'#0072BB',
        fontSize:15,
        fontWeight:'bold',
    },
    multipleChoic:{
        flexDirection:'column',
        margin:10,
        justifyContent:'flex-start'
    },
    wrapCircle1:{
        flexDirection:'row',
        margin:10
    },

}
)