import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import paymentDetail from '../views/paymentDetail';
import ubahData from '../views/ubahData';

const Stack = createNativeStackNavigator();

const navigationScreen = () => {
  return (
    <NavigationContainer>
        <Stack.Navigator initialRouteName='paymentDetail'>
            <Stack.Screen name='Payment Detail' 
            component={paymentDetail} 
            options={{ title:'Payment Detail',
            headerStyle:{backgroundColor:'#0072BB'},
            headerTitleStyle:{
                     color:'white',
            },
            headerTitleAlign:'center'
             }}/>
            <Stack.Screen
            name='Tambah Data Tamu'
            component={ubahData} 
            options={{ title:'Tambah Data Tamu',
                headerStyle:{backgroundColor:'#0072BB'},
                 headerTitleStyle:{
                        color:'white',
                },
                  headerBackTitleStyle:{
                        color:'white'
                },
                 headerTitleAlign:'center'
                 }}/>
        </Stack.Navigator>
    </NavigationContainer>
  );
};

export default navigationScreen;
