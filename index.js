/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import paymentDetail from './src/views/paymentDetail'
import ubahData from './src/views/ubahData'
import navigationScreen from './src/navigation/navigationScreen'

AppRegistry.registerComponent(appName, () => navigationScreen);
